#!/usr/bin/env runhaskell

{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleContexts #-}

module Main(main, dummyProcessFileConfig) where

import Control.Applicative(Applicative((<*>), pure), liftA2)
import Control.Monad(join)
import Control.Monad.IO.Class(MonadIO(liftIO))
import Data.Bool(bool)
import Data.Char(toLower, isSpace, digitToInt)
import Data.Maybe(fromMaybe)
import Data.Set(intersection)
import qualified Data.Set as Set(fromList)
import Data.Time(Day, formatTime, defaultTimeLocale, fromGregorian)
import GHC.IO.Encoding(setLocaleEncoding, utf8)
import System.Directory(doesDirectoryExist, listDirectory, createDirectoryIfMissing, copyFile)
import System.Environment(getArgs, lookupEnv)
import System.Exit(ExitCode(ExitFailure, ExitSuccess), exitWith)
import System.FilePath((</>), splitExtension, joinPath, splitDirectories, takeFileName, takeBaseName, addExtension, dropTrailingPathSeparator)
import System.IO(hPutStrLn, hPutStr, stderr)
import System.Process(readProcessWithExitCode)
import Text.Parsec(Stream, ParsecT, runP, digit, optional, char, many, anyChar)

-- # on build script:
-- sudo sed -i 's/\(<policy domain="coder" rights=\)"none" \(pattern="PDF" \/>\)/\1"read|write"\2/g' /etc/ImageMagick-6/policy.xml

-- # apt-get install
-- texlive-xetex cabal-install pdftohtml imagemagick libreoffice ghc

-- # cabal install
-- pandoc
main ::
  IO ()
main =
  do  setLocaleEncoding utf8
      a <- getArgs
      case a of
        title:subtitle:src:dist_dir:r ->
          do  s <- srcs src
              w <- doProcess (argsToConfig dist_dir r) s
              case w of
                ExitCodeOr (Left x) ->
                  print x *>
                  exitWith x
                ExitCodeOr (Right e) ->
                  do  i <- index title subtitle e
                      createDirectoryIfMissing True dist_dir
                      print ("writing " ++ (dist_dir </> "index.html"))
                      writeFile (dist_dir </> "index.html") i
        _ ->
          do  hPutStrLn stderr "<title> <subtitle> <src-dir|src-file> <dist-dir> [font (DejaVu Sans Mono)] [margin (1in)] [tab stop (2)] [toc-depth (4)]"
              exitWith (ExitFailure 99)

nameTable ::
  [(MetaFile, FileDescription)]
nameTable =
  [
    -- any name/date overrides
  ]

doProcess ::
  Traversable t =>
  ProcessFileConfig
  -> t FilePath
  -> IO (ExitCodeOr (t (String, [OutputFile])))
doProcess c s =
  fmap sequence (traverse (runProcessFileExit getFileDescriptionProcessed c "") s)

data ProcessFileConfig =
  ProcessFileConfig
    FilePath -- dist dir
    (Maybe String) -- the font
    (Maybe String) -- the margin
    (Maybe String) -- the tab stop
    (Maybe String) -- the TOC depth
  deriving (Eq, Show)

dummyProcessFileConfig ::
  ProcessFileConfig
dummyProcessFileConfig =
  ProcessFileConfig
    "publicx"
    Nothing
    Nothing
    Nothing
    Nothing

argsToConfig ::
  String
  -> [String]
  -> ProcessFileConfig
argsToConfig dist_dir r =
  let s = ProcessFileConfig
            dist_dir
  in  case r of
        [] ->
          s Nothing Nothing Nothing Nothing
        [font] ->
          s (Just font) Nothing Nothing Nothing
        [font, margin] ->
          s (Just font) (Just margin) Nothing Nothing
        [font, margin, tabstop] ->
          s (Just font) (Just margin) (Just tabstop) Nothing
        font:margin:tabstop:tocdepth:_ ->
          s (Just font) (Just margin) (Just tabstop) (Just tocdepth)

getSourceFileExtension ::
  Monad f =>
  ProcessFile f (Maybe String)
getSourceFileExtension =
  let dropDot ::
        String
        -> String
      dropDot ('.':z) =
        z
      dropDot x =
        x
  in  do  s <-
            getSourceProcessFile
          let (q, r) = splitExtension s
          pure $
            case q of
              [] ->
                Nothing
              _ ->
                Just (dropDot r)

getSourceFileExtensionLower ::
  Monad f =>
  ProcessFile f (Maybe String)
getSourceFileExtensionLower =
  fmap (map toLower) <$> getSourceFileExtension

getProcessForFile ::
  ProcessFileIO [OutputFile]
getProcessForFile =
  let functionTable ::
        [(String, ProcessFileIO [OutputFile])]
      functionTable =
        [
          ("markdown", markdownProcess)
        , ("md", markdownProcess)
        , ("jpeg", jpgProcess)
        , ("jpg", jpgProcess)
        , ("png", pngProcess)
        , ("svg", svgProcess)
        , ("pdf", pdfProcess)
        , ("odt", odtProcess)
        , ("fodt", odtProcess)
        , ("html", htmlProcess)
        ]
  in  do  s <-
            getSourceFileExtensionLower
          o <- fromMaybe
                ([] <$ noProcessFile)
                (s >>= \k -> lookup k functionTable)
          d <-
            getDistDirSplitProcessFile
          pure (modifyOutputFileName (\p -> joinPath (zipoff (splitDirectories p) d)) <$> o)

noProcessFile ::
  ProcessFileIO ()
noProcessFile =
  do  s <-
        getSourceFilenameProcessFile
      liftIO $
        putStrLn ("<ignoring> " ++ s)

srcs ::
  FilePath
  -> IO [FilePath]
srcs p =
  do  e <- doesDirectoryExist p
      bool
        (pure [p])
        (fmap (p </>) <$> listFilesRecursive p)
        e

getFileDescriptionProcessed ::
  ProcessFile IO (String, [OutputFile])
getFileDescriptionProcessed =
  (,) <$> getFileDescriptionDash <*> getProcessForFile

mkDocument ::
  String
  -> String
  -> String
  -> IO String
mkDocument title subtitle list =
  do  b <- htmlBottom
      pure $
        concat
          [
            htmlTop title subtitle
          , list
          , b
          ]

getDistDir ::
  ProcessFileConfig
  -> FilePath
getDistDir (ProcessFileConfig p _ _ _ _) =
  p

getFont ::
  ProcessFileConfig
  -> Maybe String
getFont (ProcessFileConfig _ t _ _ _) =
  t

getMargin ::
  ProcessFileConfig
  -> Maybe String
getMargin (ProcessFileConfig _ _ m _ _) =
  m

getTabStop ::
  ProcessFileConfig
  -> Maybe String
getTabStop (ProcessFileConfig _ _ _ s _) =
  s

getTocDepth ::
  ProcessFileConfig
  -> Maybe String
getTocDepth (ProcessFileConfig _ _ _ _ d) =
  d

newtype ExitCodeOr a =
  ExitCodeOr (Either ExitCode a)
  deriving (Eq, Show)

instance Functor ExitCodeOr where
  fmap f (ExitCodeOr x) =
    ExitCodeOr (fmap f x)

instance Applicative ExitCodeOr where
  pure =
    ExitCodeOr . pure
  ExitCodeOr (Left x) <*> _ =
    ExitCodeOr (Left x)
  ExitCodeOr (Right f) <*> ExitCodeOr a =
    ExitCodeOr (fmap f a)
    
instance Monad ExitCodeOr where
  return =
    ExitCodeOr . return
  ExitCodeOr (Left x) >>= _ =
    ExitCodeOr (Left x)
  ExitCodeOr (Right a) >>= f =
    f a

newtype ProcessFile f a =
  ProcessFile (ProcessFileConfig -> FilePath -> FilePath -> f (FilePath, ExitCodeOr a))

type ProcessFileIO a =
  ProcessFile IO a

idProcessFile ::
  Functor f =>
  (ProcessFileConfig -> f (ExitCodeOr a))
  -> ProcessFile f a
idProcessFile k =
  ProcessFile (\c _ p -> ((,) p) <$> k c)

runProcessFile ::
  ProcessFile f a
  -> ProcessFileConfig
  -> FilePath
  -> FilePath
  -> f (FilePath, ExitCodeOr a)
runProcessFile (ProcessFile k) =
  k

runProcessFileExit ::
  Functor f =>
  ProcessFile f a
  -> ProcessFileConfig
  -> FilePath
  -> FilePath
  -> f (ExitCodeOr a)
runProcessFileExit f c p q =
  snd <$> runProcessFile f c p q

instance Functor f => Functor (ProcessFile f) where
  fmap f (ProcessFile k) =
    ProcessFile (\c p -> fmap (fmap (fmap (fmap f))) (k c p))

instance Monad f => Applicative (ProcessFile f) where
  pure a =
    ProcessFile (pure (pure (\q -> pure (q, pure a))))
  ProcessFile f <*> ProcessFile a =
    ProcessFile (\c p q -> 
      f c p q >>= \(q', x) ->
      case x of
        ExitCodeOr (Left g) ->
          pure (q', ExitCodeOr (Left g))
        ExitCodeOr (Right k) ->
          a c p q' >>= \(r', y) ->
          pure (r', fmap k y)
    )

instance Monad f => Monad (ProcessFile f) where
  return a =
    ProcessFile (return (return (\p -> return (p, return a))))
  ProcessFile k >>= f =
    ProcessFile (\c p q ->
      k c p q >>= \(q', x) ->
      case x of
        ExitCodeOr (Left e) ->
          pure (q', ExitCodeOr (Left e))
        ExitCodeOr (Right a) ->
          runProcessFile (f a) c p q'
    )

instance MonadIO f => MonadIO (ProcessFile f) where
  liftIO io =
    idProcessFile (pure (liftIO ((\a -> pure a) <$> io)))

getConfigProcessFile ::
  Applicative f =>
  ProcessFile f ProcessFileConfig
getConfigProcessFile =
  idProcessFile (pure . pure)

getSourceProcessFile ::
  Applicative f =>
  ProcessFile f FilePath
getSourceProcessFile =
  ProcessFile (pure (pure (\p -> pure (p, pure p))))

modifySourceProcessFile ::
  Applicative f =>
  (FilePath -> FilePath)
  -> ProcessFile f ()
modifySourceProcessFile k =
  ProcessFile (pure (pure (\p -> pure (k p, pure ()))))

putSourceProcessFile ::
  Applicative f =>
  FilePath
  -> ProcessFile f ()
putSourceProcessFile =
  modifySourceProcessFile . const

getSourceFilenameProcessFile ::
  Applicative f =>
  ProcessFile f FilePath
getSourceFilenameProcessFile =
  takeFileName <$> getSourceProcessFile

getSourceBasenameProcessFile ::
  Applicative f =>
  ProcessFile f FilePath
getSourceBasenameProcessFile =
  takeBaseName <$> getSourceFilenameProcessFile

getDistDirProcessFile ::
  Applicative f =>
  ProcessFile f FilePath
getDistDirProcessFile =
  getDistDir <$> getConfigProcessFile

getDistDirSplitProcessFile ::
  Applicative f =>
  ProcessFile f [FilePath]
getDistDirSplitProcessFile =
  splitDirectories <$> getDistDirProcessFile

getFontProcessFile ::
  Applicative f =>
  ProcessFile f (Maybe String)
getFontProcessFile =
  getFont <$> getConfigProcessFile

getFontOrDefaultProcessFile ::
  Applicative f =>
  ProcessFile f String
getFontOrDefaultProcessFile =
  fromMaybe "\"DejaVu Sans Mono\"" <$> getFontProcessFile

getMarginProcessFile ::
  Applicative f =>
  ProcessFile f (Maybe String)
getMarginProcessFile =
  getMargin <$> getConfigProcessFile

getMarginOrDefaultProcessFile ::
  Applicative f =>
  ProcessFile f String
getMarginOrDefaultProcessFile =
  fromMaybe "1in" <$> getMarginProcessFile

getTabStopProcessFile ::
  Applicative f =>
  ProcessFile f (Maybe String)
getTabStopProcessFile =
  getTabStop <$> getConfigProcessFile

getTabStopOrDefaultProcessFile ::
  Applicative f =>
  ProcessFile f String
getTabStopOrDefaultProcessFile =
  fromMaybe "2" <$> getTabStopProcessFile

getTocDepthProcessFile ::
  Applicative f =>
  ProcessFile f (Maybe String)
getTocDepthProcessFile =
  getTocDepth <$> getConfigProcessFile

getTocDepthOrDefaultProcessFile ::
  Applicative f =>
  ProcessFile f String
getTocDepthOrDefaultProcessFile =
  fromMaybe "4" <$> getTocDepthProcessFile

getSourceToDistDirExtProcessFile ::
  Monad f =>
  String
  -> ProcessFile f FilePath
getSourceToDistDirExtProcessFile ext =
  (\s o -> o </> s ++ '.' : ext) <$>
    getSourceBasenameProcessFile <*>
    getDistDirProcessFile

getDistDirSourceBaseNameProcessFile ::
  Monad f =>
  ProcessFile f FilePath
getDistDirSourceBaseNameProcessFile =
  do  o <-
        getDistDirProcessFile
      b <-
        getSourceBasenameProcessFile
      pure (o </> b)

getSourceToDistDirProcessFile ::
  Monad f =>
  ProcessFile f FilePath
getSourceToDistDirProcessFile =
  (\s o -> o </> s) <$>
    getSourceFilenameProcessFile <*>
    getDistDirProcessFile

resultProcessFile ::
  Applicative f =>
  ExitCodeOr a
  -> ProcessFile f a
resultProcessFile e =
  ProcessFile (pure (pure (\p -> pure (p, e))))

mkDistDirProcessFile ::
  MonadIO f =>
  ProcessFile f ()
mkDistDirProcessFile =
  do  d <- getDistDirProcessFile
      liftIO (createDirectoryIfMissing True d)

exit ::
  ExitCode
  -> a
  -> ExitCodeOr a
exit x@(ExitFailure _) _ =
  ExitCodeOr (Left x)
exit ExitSuccess a =
  ExitCodeOr (Right a)

exitM ::
  Monad f =>
  f ExitCode
  -> f a
  -> f (ExitCodeOr a)
exitM x a =
  do  q <- x
      case q of
        f@(ExitFailure _) ->
          pure (ExitCodeOr (Left f))
        ExitSuccess ->
          ExitCodeOr . Right <$> a

md5sumSourceFile ::
  ProcessFile IO String
md5sumSourceFile =
  do  s <-
        getSourceProcessFile
      (x, o) <-
        liftIO $
          readProcessWithExitCodePrintError
            "md5sum"
            [s]
            []
      resultProcessFile (x `exit` takeWhile (not . isSpace) o)

getFileDescription ::
  ProcessFileIO FileDescription
getFileDescription =
  let nameFile ::
        MetaFile
        -> FileDescription
      nameFile m@(MetaFile n1 h1) =
        let match (MetaFile n2 h2) =
              n1 == n2 ||
              not (null ((Set.fromList h1) `intersection` (Set.fromList h2)))
        in  defaultNameFile m `fromMaybe` lookupBy match nameTable
  in  do  h <-
            md5sumSourceFile
          s <-
            getSourceBasenameProcessFile
          pure (nameFile (MetaFile s [h]))

getFileDescriptionDash ::
  ProcessFileIO String
getFileDescriptionDash =
  do  FileDescription t d <- getFileDescription
      pure (t ++ maybe "" (" — " ++) d)

data OutputFile =
  OutputFile
    String
    FilePath
  deriving (Eq, Show)

modifyOutputFileName ::
  (FilePath -> FilePath)
  -> OutputFile
  -> OutputFile
modifyOutputFileName k (OutputFile s n) =
  OutputFile s (k n)

getOutputFileName ::
  OutputFile
  -> FilePath
getOutputFileName (OutputFile _ n) =
  n

sameOutputFile ::
  String
  -> OutputFile
sameOutputFile s =
  OutputFile s s

pandocProcess ::
  String
  -> ProcessFileIO FilePath
pandocProcess ext =
  do  f <-
        getFontOrDefaultProcessFile
      m <-
        getMarginOrDefaultProcessFile
      p <-
        getTabStopOrDefaultProcessFile
      h <-
        getTocDepthOrDefaultProcessFile
      d <-
        getFileDescriptionDash
      s <-
        getSourceProcessFile
      o <-
        getSourceToDistDirExtProcessFile ext
      mkDistDirProcessFile
      x <- 
        liftIO $
          readProcessWithExitCodePrint
            "pandoc"
            (
              [
                "-V"
              , "geometry:margin=" ++ m
              , "--epub-chapter-level=3"
              , "--toc-depth=" ++ h
              , "--metadata"
              , "title=\"" ++ d ++ "\""        
              , "-M"
              , "mainfont=" ++ f
              , "--tab-stop=" ++ p
              , "--pdf-engine=xelatex"
              , s
              , "-o"
              , o
              ]
            )
            [] `exitM` pure o
      resultProcessFile x

pandocProcessName ::
  String
  -> String
  -> ProcessFileIO OutputFile
pandocProcessName n ext =
  OutputFile n <$> pandocProcess ext

convertProcess ::
  String
  -> ProcessFileIO FilePath
convertProcess ext =
  do  s <-
        getSourceProcessFile
      o <-
        getSourceToDistDirExtProcessFile ext
      mkDistDirProcessFile
      x <- 
        liftIO $
          readProcessWithExitCodePrint
            "convert"
            [
              s
            , o
            ]
            [] `exitM` pure o
      resultProcessFile x

convertProcessName ::
  String
  -> String
  -> ProcessFileIO OutputFile
convertProcessName n ext =
    (\p -> OutputFile n p) <$> convertProcess ext

pdftohtmlsimpleProcess ::
  ProcessFileIO FilePath
pdftohtmlsimpleProcess =
  do  s <-
        getSourceProcessFile
      o' <-
        getDistDirSourceBaseNameProcessFile
      let q = o' </> "single"
      x <- 
        liftIO $
          createDirectoryIfMissing True o' *>
          readProcessWithExitCodePrint
            "pdftohtml"
            [
              "-s"
            , s
            , q
            ]
            [] `exitM` pure (q ++ "-html.html")
      resultProcessFile x

pdftohtmlsimpleProcessName ::
  String
  -> ProcessFileIO OutputFile
pdftohtmlsimpleProcessName n =
  (\p -> OutputFile n p) <$> pdftohtmlsimpleProcess

pdftohtmlcomplexProcess ::
  ProcessFileIO FilePath
pdftohtmlcomplexProcess =
  do  s <-
        getSourceProcessFile
      b <-
        getSourceBasenameProcessFile
      o <-
        getDistDirProcessFile
      let o' = o </> b
      let q = o' </> "multi"
      x <- 
        liftIO $
          createDirectoryIfMissing True o' *>
          readProcessWithExitCodePrint
            "pdftohtml"
            [
              "-c"
            , s
            , q
            ]
            [] `exitM` pure (q ++ ".html")
      resultProcessFile x

pdftohtmlcomplexProcessName ::
  String
  -> ProcessFileIO OutputFile
pdftohtmlcomplexProcessName n =
  (\p -> OutputFile n p) <$> pdftohtmlcomplexProcess

libreofficeProcess ::
  String
  -> ProcessFileIO FilePath
libreofficeProcess ext =
  do  s <-
        getSourceProcessFile
      o <-
        getDistDirProcessFile
      o' <-
        getDistDirSourceBaseNameProcessFile
      x <- 
        liftIO $
          readProcessWithExitCodePrint
            "libreoffice"
            [
              "--invisible"
            , "--headless"
            , "--convert-to"
            , ext
            , s
            , "--outdir"
            , o
            ]
            [] `exitM` pure (addExtension o' ext)
      resultProcessFile x

libreofficeProcessName ::
  String
  -> String
  -> ProcessFileIO OutputFile
libreofficeProcessName n ext =
    (\p -> OutputFile n p) <$> libreofficeProcess ext

copyFileProcess ::
  ProcessFileIO FilePath
copyFileProcess =
  do  s <-
        getSourceProcessFile
      o <-
        getSourceToDistDirProcessFile
      mkDistDirProcessFile
      liftIO $
        putStrLn ("copyFile " ++ s ++ ' ' : o) *>
        copyFile
          s
          o
      pure o

copyFileProcessName ::
  String
  -> ProcessFileIO OutputFile
copyFileProcessName n =
  (\p -> OutputFile n p) <$> copyFileProcess

markdownProcess ::
  ProcessFile IO [OutputFile]
markdownProcess =
  do  c <-
        copyFileProcessName "md"
      k <-
        traverse (\(OutputFile n x) -> pandocProcessName x n) (sameOutputFile <$> ["epub", "pptx", "docx", "odt", "txt"])
      p <-
        pandocProcessName "pdf" "pdf"
      v <-
        pdfPostProcess (getOutputFileName p)
      pure (c : k ++ p : v)

pdfProcess ::
  ProcessFile IO [OutputFile]
pdfProcess =
  do  c <-
        copyFileProcessName "pdf"
      v <-
        pdfPostProcess (getOutputFileName c)
      pure (c:v)

pdfPostProcess ::
  FilePath
  -> ProcessFileIO [OutputFile]
pdfPostProcess p =
  do  s <- getSourceProcessFile
      putSourceProcessFile p
      q <-
        pdftohtmlsimpleProcessName "HTML single"
      putSourceProcessFile p
      r <-
        pdftohtmlcomplexProcessName "HTML multi"
      putSourceProcessFile s
      pure [q, r]

imageProcess ::
  String
  -> [OutputFile]
  -> ProcessFileIO [OutputFile]
imageProcess nm exts =
  do  c <-
        copyFileProcessName nm
      k <-
        traverse (\(OutputFile n p) -> convertProcessName n p) exts
      pure (c : k)

pngProcess ::
  ProcessFileIO [OutputFile]
pngProcess =
  imageProcess "png" (sameOutputFile <$> ["jpg", "svg"])

jpgProcess ::
  ProcessFileIO [OutputFile]
jpgProcess =
  imageProcess "jpg" (sameOutputFile <$> ["png", "svg"])

svgProcess ::
  ProcessFileIO [OutputFile]
svgProcess =
  imageProcess "svg" (sameOutputFile <$> ["jpg", "png"])

odtProcess ::
  ProcessFileIO [OutputFile]
odtProcess =
  do  c <-
        copyFileProcessName "odt"
      k <-
        traverse (\(OutputFile n p) -> libreofficeProcessName n p) (sameOutputFile <$> ["html", "docx", "txt"])
      p <-
        libreofficeProcessName "pdf" "pdf"
      v <-
        pdfPostProcess (getOutputFileName p)      
      pure (c : k ++ p : v)

htmlProcess ::
  ProcessFile IO [OutputFile]
htmlProcess =
  do  c <-
        copyFileProcessName "html" 
      k <-
        traverse (\(OutputFile n p) -> pandocProcessName n p) (sameOutputFile <$> ["epub", "pptx", "docx", "odt", "md", "txt"])
      p <-
        pandocProcessName "pdf" "pdf"
      v <-
        pdfPostProcess (getOutputFileName p)
      pure (c : k ++ p : v)

data FileDescription =
  FileDescription
    String -- name
    (Maybe String) -- date
  deriving (Eq, Show)

modFileDescriptionName ::
  (String -> String)
  -> FileDescription
  -> FileDescription
modFileDescriptionName k (FileDescription n d) =
  FileDescription (k n) d
  
data MetaFile =
  MetaFile
    String -- name
    [String] -- hashes
  deriving (Eq, Show)

readProcessWithExitCodePrint ::
  FilePath -- command
  -> [String] -- args
  -> String -- stdin
  -> IO ExitCode
readProcessWithExitCodePrint p a i =
  do  putStrLn (p ++ (a >>= \s -> ' ' : s))
      (x, o) <- readProcessWithExitCodePrintError p a i
      putStr o
      pure x

readProcessWithExitCodePrintError ::
  FilePath
  -> [String]
  -> String
  -> IO (ExitCode, String)
readProcessWithExitCodePrintError p a i =
  do  (x, o, e) <- readProcessWithExitCode p a i
      hPutStr stderr e
      pure (x, o)

defaultNameFile ::
  MetaFile
  -> FileDescription
defaultNameFile (MetaFile n _) =
  let all_lower =
        map toLower
      spacing =
        map (\c -> bool c ' ' (c `elem` "-_"))
      trim_space =
        reverse . dropWhile isSpace . reverse . dropWhile isSpace
      remove_ext ::
        String
        -> String
      remove_ext r@('.':_) =
        r
      remove_ext r =
        let br =
              break (== '.')
            remove_ext' r' =
              case br r' of
                (x, []) ->
                  x
                (x, h:y) ->
                  let nx =
                        case br y of
                          (_, []) ->
                            []
                          (x', q@(_:_)) ->
                            h : x' ++ remove_ext' q
                  in  x ++ nx
        in  remove_ext' r
      parseDay ::
        String
        -> Maybe (Day, String)
      parseDay =
        getRight . runP digits8 () "digits8"
      has_date n' =
        (\(d, r) -> FileDescription r (Just (formatTime defaultTimeLocale "%d %B %0Y" d))) <$> parseDay n'
  in  modFileDescriptionName (remove_ext . trim_space . spacing . all_lower) $
        fromMaybe
          (
            FileDescription
              n
              Nothing
          )
          (has_date n)

zipoff ::
  [a]
  -> [a]
  -> [a]
zipoff [] r =
  r
zipoff r [] =
  r
zipoff (_:x) (_:y) =
  zipoff x y

lookupBy ::
  (a -> Bool)
  -> [(a, b)]
  -> Maybe b
lookupBy _ [] =
  Nothing
lookupBy e ((a, b):t) =
  bool (lookupBy e t) (Just b) (e a)

getRight ::
  Either e a
  -> Maybe a
getRight (Left _) =
  Nothing
getRight (Right a) =
  Just a

integer ::
  (Num a, Foldable t) =>
  (Int -> a)
  -> t Char
  -> a
integer k digits =
  foldl (\x d -> 10*x + k (digitToInt d)) 0 digits

digits8 ::
  Stream s m Char =>
  ParsecT s u m (Day, String)
digits8 =
  let digitsDay d1 d2 d3 d4 d5 d6 d7 d8 =
        fromGregorian
          (integer toInteger [d1,d2,d3,d4])
          (integer id [d5,d6])
          (integer id [d7,d8])
      go8 =  
        digitsDay <$>
        digit <*>
        digit <*>
        digit <*>
        digit <*
        optional (char '-') <*>
        digit <*>
        digit <*
        optional (char '-') <*>
        digit <*>
        digit <*
        char '-'
  in  (,) <$>
      go8 <*>
      (many anyChar)

htmlTop ::
  String
  -> String
  -> String
htmlTop t s =
  "<!doctype html>\n\
  \  <html lang=\"en\">\n\
  \  <head>\n\
  \    <meta charset=\"utf-8\">\n\
  \    <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n\
  \  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n\
  \  <title>" ++ t ++ " — " ++ s ++ "</title>\n\
  \  <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic\" rel=\"stylesheet\" type=\"text/css\">\n\
  \  <link rel=\"stylesheet\" href=\"https://css.1007036.casa/bootstrap.min.css\" />\n\
  \  <link rel=\"stylesheet\" href=\"https://css.1007036.casa/font-awesome.min.css\" />\n\
  \  <link rel=\"stylesheet\" href=\"https://css.1007036.casa/custom.css\">\n\
  \  </head>\n\
  \  <body>\n\
  \    <main>\n\
  \      <div class=\"container-fluid\">\n\
  \        <div class=\"container my-4\">\n\
  \          <div class=\"row\">\n\
  \            <div class=\"col\">\n\
  \              <h1 class=\"display-4 text-center\">\n\
  \                " ++ t ++ "\n\
  \              </h1>\n\
  \            </div>\n\
  \          </div>\n\
  \        </div>\n\
  \        <div class=\"container\">\n\
  \          <div class=\"row\">\n\
  \            <div class=\"col\">\n\
  \              <div class=\"section-header text-left\">\n\
  \              <h3 class=\"bottom-line\">" ++ s ++ "</h3>\n\
  \            </div>\n\
  \            <ul class=\"list-group\">\n\
  \"

htmlBottom ::
  IO String
htmlBottom =
  do  s <- lookupEnv "CI_COMMIT_SHA"
      r <- lookupEnv "CI_PROJECT_URL"
      let d = maybe "" htmlBottomSha s
      let e = maybe "" htmlBottomRepo r
      pure $
        "              </ul>\n\
        \            </div>\n\
        \          </div>\n\
        \        </div>\n\
        \        <hr>\n\
        \" ++ d ++ "\n\
        \" ++ e ++ "\n\
        \      </div>\n\
        \    </main>\n\
        \  </body>\n\
        \</html>\n\
        \"

htmlBottomSha ::
  String
  -> String
htmlBottomSha sha =
  "<div class=\"text-center\">sha <code style=\"display: inline\">" ++ sha ++ "</code></div>"

htmlBottomRepo ::
  String
  -> String
htmlBottomRepo repo =
  "<div class=\"text-center\">repo <a href=\"" ++ repo ++ "\"><code style=\"display: inline\">" ++ repo ++ "</code></a></div>"

rowOutputFile ::
  OutputFile
  -> String
rowOutputFile (OutputFile n p) =
  "    [<a href=\"" ++ p ++ "\">" ++ n ++ "</a>]\n"

item ::
  String
  -> [OutputFile]
  -> String
item i l =
  "<li class=\"list-group-item\">\n\
  \  <span>" ++ i ++ "</span>\n\
  \  <span class=\"float-right\">\n\
  \" ++ (l >>= rowOutputFile) ++ "\
  \  </span>\n\
  \</li>\n"

index ::
  String
  -> String
  -> [(String, [OutputFile])]
  -> IO String
index title subtitle e =
  mkDocument title subtitle (e >>= uncurry item)


----

partitionM ::
  Applicative m =>
  (a -> m Bool)
  -> [a]
  -> m ([a], [a])
partitionM _ [] =
  pure ([], [])
partitionM f (x:xs) =
  liftA2
    (\res (as, bs) -> 
      let onres p q =
            bool p q res
      in  (onres id (x:) as, onres (x:) id bs)
    )
    (f x)
    (partitionM f xs)
  
listFilesInside ::
  (FilePath -> IO Bool)
  -> FilePath
  -> FilePath
  -> IO [FilePath]
listFilesInside test base dx =
    bool'
      (return [])
      (
        let listFilesInside' dir =
              let dir' =
                    base </> dir
              in  do  (dirs,files) <- partitionM (\d -> doesDirectoryExist (dir' </> d)) =<< listDirectory dir'
                      rest <- join <$> traverse (\d -> listFilesInside' (dir </> d)) dirs
                      return (((dir </>) <$> files) ++ rest)
        in  listFilesInside' dx
      )
      (test $ dropTrailingPathSeparator dx)

listFilesRecursive ::
  FilePath
  -> IO [FilePath]
listFilesRecursive b =
  listFilesInside (const $ return True) b ""


bool' ::
  Monad f =>
  f a
  -> f a
  -> f Bool
  -> f a
bool' f t p =
  p >>= bool f t
